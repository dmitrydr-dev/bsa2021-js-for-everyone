import { controls } from '../../constants/controls';

// DONE create new state object
const fightingState = new Map();

export async function fight(firstFighter, secondFighter) {
  keyboardManipulation(firstFighter, secondFighter);

  // DONE fill the state with health of each fighter & necessary values
  fightingState.set(firstFighter._id, firstFighter.health);
  fightingState.set(secondFighter._id, secondFighter.health);
  fightingState.set('isGameOver', false);
  fightingState.set('winner', null);

  return new Promise((resolve) => {
    // resolve the promise with the winner when fight is over
    // DONE waiting for the getWinner implementation
    resolve(getWinner());
  });
}

export function getDamage(attacker, defender) {
  // return damage
  const hitPower = getHitPower(attacker);
  const blockPower = getBlockPower(defender);

  if (blockPower > hitPower) {
    return 0;
  }

  return hitPower - blockPower;
}

// DONE create throttled functions to handle critical damages
const throttledGetCriticalDamage = throttle(getCriticalDamage, 10000);

function getCriticalDamage(attacker, defender, healthBar) {
  const damage = attacker.attack * 2;

  calculateHealth(defender, damage, healthBar, attacker);
}

export function getHitPower(fighter) {
  // DONE return hit power
  const { attack } = fighter;
  const criticalHitChance = randomNumber(1, 2);
  const power = attack * criticalHitChance;
  return power;
}

export function getBlockPower(fighter) {
  // DONE return block power
  const { defense } = fighter;
  const dodgeChance = randomNumber(1, 2);
  const power = defense * dodgeChance;
  return power;
}

export function randomNumber(min, max) {
  return Math.random() * (max - min + 1) + min;
}

function keyboardManipulation(firstFighter, secondFighter) {
  const firstFighterHealthBar = document.querySelector('#left-fighter-indicator');
  const secondFighterHealthBar = document.querySelector('#right-fighter-indicator');
  const allCombinations = new Map(Object.entries(controls));
  const pressedKeys = new Set();

  document.addEventListener('keydown', function (event) {
    pressedKeys.add(event.code);
    const combinations = defineAction(pressedKeys, allCombinations);
    getAction(combinations, firstFighter, secondFighter, firstFighterHealthBar, secondFighterHealthBar);
  });
  document.addEventListener('keyup', function (event) {
    pressedKeys.delete(event.code);
  });
}

function defineAction(currentlyPressedKeys, allCombinations) {
  const combinations = new Set();

  for (let [key, value] of allCombinations) {
    const normalizedValue = !Array.isArray(value) ? [value] : value;

    if (currentlyPressedKeys.has(...normalizedValue)) {
      combinations.add(key);
    }
  }

  return combinations;
}

// DONE create action handler

function getAction(combinations, firstFighter, secondFighter, firstFighterHealthBar, secondFighterHealthBar) {
  if (combinations.has('PlayerOneAttack' && 'PlayerTwoBlock')) {
    return;
  }

  if (combinations.has('PlayerTwoAttack' && 'PlayerOneBlock')) {
    return;
  }

  if (combinations.has('PlayerOneAttack' && 'PlayerOneBlock')) {
    return;
  }

  if (combinations.has('PlayerTwoAttack' && 'PlayerTwoBlock')) {
    return;
  }

  if (combinations.has('PlayerOneAttack')) {
    const damage = getDamage(firstFighter, secondFighter);
    calculateHealth(secondFighter, damage, secondFighterHealthBar, firstFighter);
    return;
  }

  if (combinations.has('PlayerTwoAttack')) {
    const damage = getDamage(secondFighter, firstFighter);
    calculateHealth(firstFighter, damage, firstFighterHealthBar, secondFighter);
    return;
  }

  if (combinations.has('PlayerOneCriticalHitCombination')) {
    throttledGetCriticalDamage(firstFighter, secondFighter, secondFighterHealthBar);
    return;
  }

  if (combinations.has('PlayerTwoCriticalHitCombination')) {
    throttledGetCriticalDamage(secondFighter, firstFighter, firstFighterHealthBar);
    return;
  }
}

// DONE health calculation function

function calculateHealth(injuredFighter, damage, healthBar, opponent) {
  const injuredFighterHealth = fightingState.get(injuredFighter._id);
  const newInjuredFighterHealth = injuredFighterHealth - damage;
  fightingState.set(injuredFighter._id, newInjuredFighterHealth);

  calculateHeathBarLength(injuredFighter, newInjuredFighterHealth, healthBar);

  if (newInjuredFighterHealth <= 0) {
    fightingState.set('winner', opponent.name);
    fightingState.set('isGameOver', true);
  }
}

// DONE

function getWinner() {
  return new Promise((resolve) => {
    const timerId = setInterval(() => {
      if (fightingState.get('isGameOver')) {
        const winner = fightingState.get('winner');
        resolve({ winner: winner, id: timerId });
      }
    }, 100);
  });
}

function throttle(func, ms) {
  let isThrottled = false;
  let savedArgs;
  let savedThis;

  function wrapper() {
    if (isThrottled) {
      savedArgs = arguments;
      savedThis = this;
      return;
    }

    func.apply(this, arguments);

    isThrottled = true;

    setTimeout(function () {
      isThrottled = false;
    }, ms);
  }

  return wrapper;
}

function calculateHeathBarLength(fighter, newHealthStatus, healthBar) {
  const length = (newHealthStatus * 100) / fighter.health;

  healthBar.style.width = `${length}%`;
}
