import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`
  });

  // todo: show fighter info (image, name, health, etc.)

  if (fighter) {
    const fighterFeatures = createFighterPreviewCard(fighter);
    fighterElement.append(fighterFeatures);
  }

  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = {
    src: source,
    title: name,
    alt: name
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes
  });

  return imgElement;
}

export function createFighterPreviewCard(fighter) {
  if (!fighter) {
    return;
  }

  const { attack = 'n/a', defense = 'n/a', health = 'n/a', name = 'Anonymous' } = fighter;
  const fighterCard = createElement({
    tagName: 'div',
    className: 'fighter-preview__card'
  });

  // image
  const fighterImage = createFighterImage(fighter);

  const fighterImageWrap = createElement({
    tagName: 'div',
    className: 'fighter-preview__img-wrap'
  });

  // name
  const fighterName = createElement({
    tagName: 'div',
    className: 'fighter-preview__name'
  });
  fighterName.innerText = `Name: ${name}`;
  // attack
  const fighterAttack = createElement({
    tagName: 'div',
    className: 'fighter-preview__property'
  });
  fighterAttack.innerText = `Attack: ${attack}`;
  // defense
  const fighterDefense = createElement({
    tagName: 'div',
    className: 'fighter-preview__property'
  });
  fighterDefense.innerText = `Defense: ${defense}`;
  // health
  const fighterHealth = createElement({
    tagName: 'div',
    className: 'fighter-preview__property'
  });
  fighterHealth.innerText = `Health: ${health}`;

  fighterImageWrap.append(fighterImage);

  const fighterFeatures = [fighterImageWrap, fighterName, fighterAttack, fighterDefense, fighterHealth];

  fighterCard.append(...fighterFeatures);

  return fighterCard;
}
