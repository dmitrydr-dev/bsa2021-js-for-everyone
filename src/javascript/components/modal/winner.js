import { showModal } from './modal';

export function showWinnerModal(fighter) {
  // call showModal function
  showModal({
    title: `Game Over!`,
    bodyElement: `The winner is ${fighter}!`
  });
}
